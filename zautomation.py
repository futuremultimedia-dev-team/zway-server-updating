from __future__ import print_function
import requests
from requests.auth import HTTPBasicAuth
import config
import json
import commands
import time
import sys
from bunch import bunchify

def printConfig():
    print("Zway Configuration:")
    print(" API_url="+config.API_url)
    print(" API_user="+config.API_user)
    print(" API_pass="+config.API_pass)
    return

def run(apiCommand):
    url=config.API_url + "/" + apiCommand
    try:
        resp=requests.get(url,auth=HTTPBasicAuth(config.API_user, config.API_pass))
        if resp.status_code != 200:
            return ""
        else:
            resp=resp.json()
            resp=bunchify(resp)
            return resp
    except Exception as e:
        print("ERROR: " + format(e))
        #raise

def runRemote(apiCommand,API_url):
    url=API_url + "/" + apiCommand
    try:
        resp=requests.get(url,auth=HTTPBasicAuth(config.API_user, config.API_pass))
        if resp.status_code != 200:
            return ""
        else:
            resp=resp.json()
            resp=bunchify(resp)
            return resp
    except Exception as e:
        print("ERROR: " + format(e))
        #raise

def loadDevice(devId):
    return run("devices/"+str(devId)+"/")

def loadDevices():
    devices=run("devices")
    devices=devices.data.devices
    devices=sortDevices(devices)
    return devices


def sortDevices(unordered_devices):
    return unordered_devices
    ordered_devices=[]
    keys=sorted(unordered_devices.keys(), key=int)
    for key in keys:
    	ordered_devices.append(unordered_devices[key])
    return ordered_devices



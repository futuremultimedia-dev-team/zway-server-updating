#!/usr/bin/env python
from __future__ import print_function
import json
import time
import os
import commands
import zautomation
import sys
import traceback
import thread

running = []


def remote_dev(devId):
	API_url=""
        if devId.find("RemoteHA_197") >-1:
                API_url="http://192.168.60.197"
                r_devid=devId.replace("RemoteHA_197_","")
        if devId.find("RemoteHA_199") >-1:
                API_url="http://192.168.60.199"
                r_devid=devId.replace("RemoteHA_199_","")
        if devId.find("RemoteHA_203") >-1:
                API_url="http://192.168.60.203"
                r_devid=devId.replace("RemoteHA_203_","")
        if devId.find("RemoteHA_205") >-1:
                API_url="http://192.168.60.205"
                r_devid=devId.replace("RemoteHA_205_","")
        if devId.find("RemoteHA_206") >-1:
                API_url="http://192.168.60.206"
                r_devid=devId.replace("RemoteHA_206_","")
        if devId.find("RemoteHA_209") >-1:
                API_url="http://192.168.60.209"
                r_devid=devId.replace("RemoteHA_209_","")
        if devId.find("RemoteHA_208") >-1:
                API_url="http://192.168.60.208"
                r_devid=devId.replace("RemoteHA_208_","")

	if API_url =="":
		raise Exception("Error decoding remote device: " + devId)

	API_url += ":8083/ZAutomation/api/v1"
	rdev=zautomation.runRemote("devices/"+r_devid,API_url).data

	return rdev
	

def check_again(localId,localVal,attempts=0):
	print("Checking again " + localId)
	dev=zautomation.run("devices/"+localId).data
	rdev=remote_dev(localId)

	print(rdev.id.ljust(35," "), end=" - ")
	print(rdev.metrics.title.encode('ascii','ignore').ljust(50," "), end=" - ")
	print(str(rdev.metrics.level).ljust(5," "), end= " - ")
	if dev.metrics.level != rdev.metrics.level:
		if attempts % 5 == 0:
			print("STILL NOT IN SYNC (" + str(attempts) + ")")
			zautomation.run("devices/"+dev.id+"/command/update")
		attempts +=1
		time.sleep(1)
		thread.start_new_thread( check_again, (dev.id, dev.metrics.level, attempts ) )
	else:
		print("IN SYNC AGAIN (" + str(attempts) + ")")
		running.remove(localId)
	print("Completato CHECK")

while True:
	try:
		print("Loading devices...")
		devices=zautomation.loadDevices();
		notinsync=0
		errors=0
		time.sleep(1)
		for dev in devices:
			if dev.deviceType == "switchBinary" or dev.deviceType == "switchMultilevel":
				if dev.id.find("RemoteHA") < 0:
					continue	

				print(dev.id.ljust(35," "), end=" - ")
				print(dev.metrics.title.encode('ascii','ignore').ljust(50," "), end=" - ")

				print(str(dev.metrics.level).ljust(5," "), end= "")
				print("")

				try:			
					rdev=remote_dev(dev.id)
					#print(rdev)
					print(rdev.id.ljust(35," "), end=" - ")
					print(rdev.metrics.title.encode('ascii','ignore').ljust(50," "), end=" - ")
					print(str(rdev.metrics.level).ljust(5," "), end= " - ")
					print(str(dev.metrics.level).ljust(5," "), end=" - ")

					time.sleep(0.5)
					
					if dev.metrics.level != rdev.metrics.level:
						if dev.id in running:
							print("SYNCING")
							continue
						print("")
						print("NOT IN SYNC")
						running.append(dev.id)
						time.sleep(5)
						thread.start_new_thread( check_again, (dev.id, dev.metrics.level ) )
						notinsync += 1
						zautomation.run("devices/"+dev.id+"/command/update")
						time.sleep(5)
				except Exception, e:
					print("Dev Error: " + str(sys.exc_info()[0]) + " " + str(e))
			                traceback.print_exc()
					errors += 1
					time.sleep(10)
				print("")
				print("")
				sys.stdout.flush()			
		if notinsync >0:
			print("NOT IN SYNC: " + str(notinsync))
		if errors >0:
			print("ERRORS: " + str(errors))
		print("----------------------------------------------------------------------------------------")	
		sys.stdout.flush()			
		time.sleep(1)
	except Exception, e:
		print("General Error: " + str(sys.exc_info()[0]) + " " + str(e))
		traceback.print_exc()
		print("Retrying in 30 sec....")
		time.sleep(30)
